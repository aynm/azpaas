﻿
$Env='Dev'
$Location = 'SouthCentralUS'
$RG= New-AzResourceGroup -Location $Location -Name RG-Ztech-$Env-002 -Verbose

$SA= New-AzStorageAccount -Location $Location -ResourceGroupName $RG.ResourceGroupName -Name mxsaztech$($Env.ToLower())002 -SkuName Standard_LRS -AccessTier Hot -Kind StorageV2 -EnableHttpsTrafficOnly 1 -Verbose
$KV= New-AzKeyVault -Location $Location -ResourceGroupName $RG.ResourceGroupName -VaultName KV-Ztech-$Env-002 -Sku Premium -Verbose

#Creates consumption tier functions app. However, can't control the Function app plan name. Cnd creates an App insights instance inspite of -DisableApplicationInsights
New-AzFunctionApp -Location $Location -ResourceGroupName $RG.ResourceGroupName -Name Func-Ztech-$Env-002  -StorageAccount $SA.StorageAccountName -OSType Windows -Runtime PowerShell `
                  -RuntimeVersion 7.0  -FunctionsVersion 3 -DisableApplicationInsights -Verbose 
 

#Consumption tier can't connect to vNet isolated Functions
$APIM= New-AzApiManagement -Location $Location -ResourceGroupName $RG.ResourceGroupName -Name APIM-Ztech-$Env-002 -Organization mullick.in -AdminEmail ayan@mullick.in -Sku Developer -Verbose


$Password=[System.Web.Security.Membership]::GeneratePassword(8,1)
$Secret = ConvertTo-SecureString -String $Password -AsPlainText -Force
$Cred = New-Object System.Management.Automation.PSCredential "SQLadmin",$Secret
Set-AzKeyVaultSecret -VaultName $Kv.VaultName -Name SQL-Ztech-$($Env.ToLower())-002 -SecretValue $Secret -Verbose
New-AzSqlServer -Location $Location -ResourceGroupName $RG.ResourceGroupName  -ServerName sql-ztech-$($Env.ToLower())-002 -ServerVersion 12.0 -SqlAdministratorCredentials $Cred -Verbose
New-AzSqlDatabase -ResourceGroupName $RG.ResourceGroupName -ServerName sql-ztech-$($Env.ToLower())-002 -DatabaseName SQLdb-Ztech-$Env-002 -ComputeGeneration Gen5 -ComputeModel Serverless -Edition GeneralPurpose -VCore 1 -Verbose


#region Network Deployment
#Enable AAD auth for VPN client
#https://login.microsoftonline.com/common/oauth2/authorize?client_id=41b23e61-6c1e-4545-b367-cd054e0ed4b4&response_type=code&redirect_uri=https://portal.azure.com&nonce=1234&prompt=admin_consent

$vnet=New-AzVirtualNetwork -Location $Location -ResourceGroupName $RG.ResourceGroupName -Name VNET-Ztech-$Env-001 -AddressPrefix 192.168.1.0/26 -Verbose

Add-AzVirtualNetworkSubnetConfig -Name GatewaySubnet -VirtualNetwork $vnet -AddressPrefix 192.168.1.0/28 -Verbose
Add-AzVirtualNetworkSubnetConfig -Name PersonalizerSubnet -VirtualNetwork $vnet -AddressPrefix 192.168.1.32/27 -Verbose  #A minimum of 32 IP's needed for Function app's vNet integration

Set-AzVirtualNetwork -VirtualNetwork $vnet -Verbose

$gwpip    =New-AzPublicIpAddress -Location $Location -ResourceGroupName $RG.ResourceGroupName -Name PIP-Ztech-$Env-001 -AllocationMethod Dynamic -Verbose
#$subnet   = Get-AzVirtualNetworkSubnetConfig -VirtualNetwork $vnet -Name GatewaySubnet  -Verbose   won't work subnets weren't created when the vnet variable was created

$gwipconf = New-AzVirtualNetworkGatewayIpConfig -Subnet (Get-AzVirtualNetwork -ResourceGroupName RG-Ztech-Dev-001 -Name VNET-Ztech-Dev-001).Subnets[0] -Name GWIPconfName -PublicIpAddress $gwpip -Verbose

#Generate 2 will need Gw2 sku and would cost more
$gw =New-AzVirtualNetworkGateway -Location $Location -ResourceGroupName $RG.ResourceGroupName -Name GW-Ztech-$Env-001 -IpConfigurations $gwipconf -GatewayType Vpn -VpnType RouteBased -GatewaySku VpnGw1 -VpnGatewayGeneration Generation1 -Verbose


#The audience id is the same for all. The tenant and issuer uri's use the tenant id
Set-AzVirtualNetworkGateway -VirtualNetworkGateway $gw -VpnClientAddressPool 192.168.2.0/28 -VpnClientProtocol OpenVPN -AadTenantUri "https://login.microsoftonline.com/1054d918-a880-46d0-aa19-2dd47c7588d3/" `
                -AadAudienceId "41b23e61-6c1e-4545-b367-cd054e0ed4b4" -AadIssuerUri "https://sts.windows.net/1054d918-a880-46d0-aa19-2dd47c7588d3/" -Verbose

#endregion


#region custom advertisement of service tags
(Get-AzWebApp -ResourceGroup RG-Ztech-Dev-001 -name ZTechStandard).PossibleOutboundIpAddresses
$a= ('52.171.218.239/32','52.171.223.92/32','52.171.217.31/32','52.171.216.93/32','52.171.220.134/32','40.84.145.193/32','23.98.149.67/32','23.98.148.122/32')

$b= ((Get-AzNetworkServiceTag -Location 'South Central US').Values|? Name -EQ AppService.SouthCentralUS).properties.AddressPrefixes

#To compare $a and $b. Not needed eventually
$a | ForEach-Object {
    if ($b -contains $_) {
        Write-Host "`$array2 contains the `$array1 string [$_]"
    }
}

$c= ('52.171.223.92/32','52.171.217.31/32','52.171.216.93/32','52.171.220.134/32','40.84.145.193/32','23.98.149.67/32','23.98.148.122/32')

$k=((Get-AzNetworkServiceTag -Location SouthCentralUS).Values|? Name -EQ AzureKeyVault.SouthCentralUS).properties.AddressPrefixes
$s=((Get-AzNetworkServiceTag -Location SouthCentralUS).Values|? Name -EQ Storage.SouthCentralUS).properties.AddressPrefixes
$sql=((Get-AzNetworkServiceTag -Location SouthCentralUS).Values|? Name -EQ SQL.SouthCentralUS).properties.AddressPrefixes
$databricks=((Get-AzNetworkServiceTag -Location SouthCentralUS).Values|? Name -EQ AzureDatabricks).properties.AddressPrefixes
$datafactory=((Get-AzNetworkServiceTag -Location SouthCentralUS).Values|? Name -EQ DataFactory.SouthCentralUS).properties.AddressPrefixes



$gw = Get-AzVirtualNetworkGateway -ResourceGroupName RG-Ztech-Dev-001 -Name GW-Ztech-Dev-001 
Set-AzVirtualNetworkGateway -VirtualNetworkGateway $gw -CustomRoute $($b+$k+$s) -Verbose   #Works for consumption tier App Servce

#endregion



#Download profile on the client
$profile=Get-AzVpnClientConfiguration -ResourceGroupName RG-Ztech-Dev-001 -Name GW-Ztech-Dev-001 -Verbose 
$profile.VPNProfileSASUrl


#region Network configuration
$SubnetId=(Get-AzVirtualNetworkSubnetConfig -VirtualNetwork $(Get-AzVirtualNetwork -ResourceGroupName RG-Ztech-Dev-001 -Name VNET-Ztech-Dev-001) -Name GatewaySubnet).id  #the subnet should have sql endpoint enabled
Set-AzVirtualNetworkSubnetConfig -VirtualNetwork $vnet -Name GatewaySubnet -ServiceEndpoint Microsoft.Sql,Microsoft.Web,Microsoft.Storage,Microsoft.KeyVault -AddressPrefix 192.168.1.0/28 -Verbose
Set-AzVirtualNetwork -VirtualNetwork $vnet -Verbose


#Failed: Long running operation failed with status 'BadRequest'. Had to do manually
$APIM.VpnType = "External"
$APIM.VirtualNetwork = New-AzApiManagementVirtualNetwork -SubnetResourceId $SubnetId -Verbose
Set-AzApiManagement -InputObject $APIM -Verbose

#Webapp
Add-AzWebAppAccessRestrictionRule -ResourceGroupName $RG.ResourceGroupName -WebAppName Func-Ztech-$Env-002 -SubnetId $SubnetId -Name vNetIsolation -Priority 100 -Action Allow -Verbose



#SQL
Set-AzSqlServerActiveDirectoryAdministrator -ResourceGroupName $RG.ResourceGroupName -ServerName sql-ztech-$($Env.ToLower())-002 -DisplayName ayan@mullick.in -Verbose 
New-AzSqlServerVirtualNetworkRule -ResourceGroupName $RG.ResourceGroupName -ServerName sql-ztech-$($Env.ToLower())-002  -VirtualNetworkRuleName vNetIsolation -VirtualNetworkSubnetId $SubnetId -Verbose   

#Storage
Get-AzStorageAccountNetworkRuleSet -ResourceGroupName $RG.ResourceGroupName -AccountName $SA.StorageAccountName   #Check for existing Network rules
(Get-AzStorageAccountNetworkRuleSet -ResourceGroupName $RG.ResourceGroupName -AccountName $SA.StorageAccountName).VirtualNetworkRules
Update-AzStorageAccountNetworkRuleSet -ResourceGroupName $RG.ResourceGroupName -AccountName $SA.StorageAccountName -DefaultAction Deny -Verbose #The rule shows up on the portal if the default action is set to Deny
Add-AzStorageAccountNetworkRule -ResourceGroupName $RG.ResourceGroupName -AccountName $SA.StorageAccountName -VirtualNetworkResourceId $SubnetId -Verbose

#KeyVault
Add-AzKeyVaultNetworkRule -VaultName $KV.VaultName -VirtualNetworkResourceId $SubnetId -PassThru -Verbose  #Worked But still had to select 'selected networks' in the 'networks' blade of the KeyVault

#endregion


#region Governance configuration 
#Enable HTTPS for AzFunctions
$app = Get-AzWebApp -ResourceGroupName $RG.ResourceGroupName -Name Func-Ztech-$Env-002
#$app.SiteConfig.AlwaysOn = $true
$app.SiteConfig.Use32BitWorkerProcess = $false
$app.HttpsOnly= $true
$app.SiteConfig.Http20Enabled= $true
$app | Set-AzWebApp


#configure AzFunctions custom domain
Resolve-DnsName -Type SOA -Name ztech.net -Verbose  #Checks who the DNS provider is for the domain
Select-AzSubscription -Subscription c2d7e81b-ed6a-4de9-a4cd-36e679ec4259
#One still needed to get the value for the text record from the Azure portal
New-AzDnsRecordSet -ResourceGroupName ayan -ZoneName ayn.org.uk -Name asuid.Func-Ztech-Dev-002 -RecordType TXT -DnsRecords $(New-AzDnsRecordConfig -Value FC647DB69107FC785FD90B598AF73D0E47002E29DC7C76B5E813DF8346426964) -Ttl 3600 -Verbose
New-AzDnsRecordSet -ResourceGroupName ayan -ZoneName ayn.org.uk -Name Func-Ztech-Dev-002 -RecordType CNAME -DnsRecords $(New-AzDnsRecordConfig -Cname func-ztech-dev-002.azurewebsites.net ) -Ttl 3600 -Verbose

Select-AzSubscription -Subscription fteinternal
Set-AzWebApp -ResourceGroupName $RG.ResourceGroupName -Name Func-Ztech-$Env-002  -HostNames @("Func-Ztech-$Env-002.ayn.org.uk","Func-Ztech-$Env-002.azurewebsites.net") -Verbose  #Sets custom domain
New-AzResourceGroupDeployment -ResourceGroupName $RG.ResourceGroupName -TemplateFile 'DevConsumptionFuncCert.json' -Verbose  #Worked. Creates AppService managed certificate
New-AzWebAppSSLBinding -ResourceGroupName $RG.ResourceGroupName -WebAppName Func-Ztech-Dev-002 -Name func-ztech-dev-002.ayn.org.uk -Thumbprint $(Get-AzWebAppCertificate -ResourceGroupName RG-Ztech-Dev-002).Thumbprint -Verbose #Does the binding





#keyvault RBAC enable
($resource = Get-AzResource -ResourceId (Get-AzKeyVault -VaultName $KV.VaultName).ResourceId).Properties | Add-Member -MemberType "NoteProperty" -Name "EnableRbacAuthorization" -Value "true" -Force
Set-AzResource -resourceid $KV.ResourceId -Properties $resource.Properties -Force -Verbose
#endregion

Add identity for azure function

#New-AzDnsRecordSet -ResourceGroupName ""  -ZoneName "" -Name "" -TargetResourceId ""    Try with target resource id
  


SQL custom domain
Minimum TLS version
Allow Azure services
Azure SQL Auditing
Defender for SQL


